﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastSpeed : MonoBehaviour

{
    [SerializeField] int fastSpeed;

    public int GetFastSpeed()
    {
        return fastSpeed;
    }
    
}
