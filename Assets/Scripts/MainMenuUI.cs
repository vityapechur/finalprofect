﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuUI : MonoBehaviour
{
  /*  [SerializeField] int currenntUnlockedLevel=1  ;*/

   /* [Header("Levels selecter")]
    [SerializeField] GameObject levels;*/
    SceneLoader sceneLoader;
   
 
    [SerializeField] Button continueGame;
    [SerializeField] Button exitGame;
    [SerializeField] Button returnInMenu;
    

    [SerializeField] bool touchPause;

    // Start is called before the first frame update
    void Start()
    {
        sceneLoader = FindObjectOfType<SceneLoader>();

       

        
        continueGame.gameObject.SetActive(false);
        returnInMenu.gameObject.SetActive(false);
        exitGame.gameObject.SetActive(false);

      


        /* for (int i = 0; i <= levels.transform.childCount; i++)
         {
             Transform child = levels.transform.GetChild(i);
             if (i + 1 > currenntUnlockedLevel)
             {
                 Button button =child.GetComponent<Button>();
                 button.interactable = true;
             }
         }*/
    }

    public void ExitGame()
    {
        sceneLoader.ExitGame();
    }

   

    public void LoadScene(int buildIndex )
    {
        SceneManager.LoadScene(buildIndex);
    }

    public void Pause()
    {
       
            Time.timeScale = 0;
         
            continueGame.gameObject.SetActive(true);
            exitGame.gameObject.SetActive(true);
            returnInMenu.gameObject.SetActive(true);
            touchPause = true;
            Debug.Log("true");
        
       
    }
    public void PauseStop()
    {
        

            Time.timeScale = 1;
            continueGame.gameObject.SetActive(false);
            exitGame.gameObject.SetActive(false);
            returnInMenu.gameObject.SetActive(false);
            touchPause = false;
            Debug.Log("false");
        

    }
    // Update is called once per frame

}
