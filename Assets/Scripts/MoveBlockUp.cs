﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBlockUp : MonoBehaviour
{
    [SerializeField] float wanderingDistance;
    [SerializeField] float speed;
    Vector2 target;
    // Start is called before the first frame update
    void Start()
    {
        target = new Vector2(transform.position.x , transform.position.y+ wanderingDistance * speed);
        wanderingDistance = -wanderingDistance;
    }

    // Update is called once per frame
    void Update()
    {
        float distanceWandering = Vector2.Distance(transform.position, target);
        transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);
        if (distanceWandering <= 0.1f)
        {
            Start();
        }

    }
}
