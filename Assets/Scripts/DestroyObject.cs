﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour


{
    PlayerController playerController;
    Coroutine destroingCoroutine;
    [SerializeField] float lifeTime;

    private void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
    }
    // Start is called before the first frame update
    

    IEnumerator Destroing()
    {
       

        yield return new WaitForSeconds(lifeTime);
        Destroy(gameObject);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            destroingCoroutine = StartCoroutine(Destroing());
            
        }
    }
}
