﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coins : MonoBehaviour
{
    [SerializeField] int coins;
    [SerializeField] AudioClip sound;

    PlayerController playerController;
    
    private void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
    }
    public int GetCoin()
    {
        return coins;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
            Destroy(gameObject);
        playerController.PlaySound(sound);
        Debug.Log("Sound");
    }
}
