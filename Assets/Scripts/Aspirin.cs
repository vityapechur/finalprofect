﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aspirin : MonoBehaviour
{
    // Start is called before the first frame update
    PlayerController playerController;
    
    
    void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
        
    }

    // Update is called once per frame
  
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerController.Recovery();
            Debug.Log("Enter");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerController.Damage();
            Debug.Log("Exit");
        }
    }
}
