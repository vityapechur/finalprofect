﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] Slider healthBar;
   
    PlayerController playerController;
    // Start is called before the first frame update
    void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
        healthBar.maxValue = playerController.GetMaxHealth();
        
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.value = playerController.GetHealth();
        

    }
}
