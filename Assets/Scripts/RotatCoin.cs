﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatCoin : MonoBehaviour
{
    [SerializeField] float rotSpeed = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

   
    
    void Update()
    {
        transform.rotation = transform.rotation * Quaternion.Euler(0, rotSpeed, 0);
    }
}
