﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{

    PlayerController playerController;
    MainMenuUI mainMenuUI;
    private void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
        mainMenuUI = GetComponent<MainMenuUI>();
    }
    void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ExitGame();
            }
        }

        private void OnDestroy()
        {
           // Debug.Log("Destroy: " + variable);
        }

        public void LoadNextScene()
        {
            Scene activeScene = SceneManager.GetActiveScene();
            int currentSceneIndex = activeScene.buildIndex;
            SceneManager.LoadScene(currentSceneIndex + 1);
       
    }

        public void LoadNextSceneWithDelay()
        {
            Invoke("LoadNextScene", 1.2f);
        }

        public void LoadCurrentScene()
        {
        SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex ) ;
        }
    public void LoadCurrentSceneWithDelay()
    {
        Invoke("LoadCurrentScene", 1.2f);
    }

    public void LoadFirstScene()
    {
        SceneManager.LoadScene(1);
    }
    public void LoadSecondScene()
    {
        SceneManager.LoadScene(2);
    }
    public void LoadThirdScene()
    {
        SceneManager.LoadScene(3);

    }

    public void LoadFourthScene()
    {
        SceneManager.LoadScene(4);
    }

    public void LoadFivethScene()
    {
        SceneManager.LoadScene(5);
    }




    public void ExitGame()
        {
            Application.Quit();
        }

}
