﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
    [SerializeField] int speed;
    [SerializeField] int currentSpeed;
  
    [SerializeField] float health = 100;
    [SerializeField] float damageMyself=1;
    [SerializeField] float damageWhenFire;
    [SerializeField] float fireRate = 0.1f;
    [SerializeField] GameObject bullet;
    [SerializeField] Transform gunPosition;
    [SerializeField] float jumpForce;
    [SerializeField] float jumpRate;
    [SerializeField ]bool jump=true;
    [SerializeField] float nextFire;
    [SerializeField] int score;
    [SerializeField] Text scoreText;
    [SerializeField] AudioClip[] sounds;
 

    float maxHealth = 100;

    AudioSource audioSource;

    float percent;
  
    bool checkDamage;
    private SpriteRenderer mySpriteRenderer;

    Rigidbody2D rigidbody;
    public Vector2 heightJump;
    DamageDealer damageDealer;
    SceneLoader sceneLoader;
    Coins coins;

    Coroutine shootingCoroutine;
    Coroutine jumpingCoroutine;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        coins = FindObjectOfType<Coins>();
        sceneLoader = FindObjectOfType<SceneLoader>();
       checkDamage = true;
        rigidbody = GetComponent<Rigidbody2D>();
       
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Shoot();
        CheckDamage();
        Jump();
       
      
       
        
    }
    public void PlaySound(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }
    public float GetMaxHealth()
    {
        return maxHealth;
    }
   
    public float GetHealth()
    {
        return health;
    }

    public float GetPercent()
    {
        percent = health / maxHealth;
        return percent;
    }
    public float GetHealthMyself()
    {
        return damageMyself;
    }

    public void Score()
    {
        
    }

    public void  Damage()
    {
        checkDamage = true;
        health = health - damageMyself * Time.deltaTime;
        
      


      
        if (health <= 0)
        {
            sceneLoader.LoadCurrentSceneWithDelay();
        }

    }

    public void DamageWhenFire()
    {
      
        health = health - damageWhenFire;
      
    }
     public void  Recovery()
    {
        checkDamage = false;
        if (health <= 100)
        {
            health = health + damageMyself * Time.deltaTime*10;
        }
    }

    public void CheckDamage()
    
    {
        if (checkDamage == true)
        {
            Damage();
        }
        else 
        {
            Recovery();
        }
    }

    public void Move()
    {
        Vector3 currentPos = transform.position;
        float inputX = Input.GetAxis("Horizontal");
        rigidbody.velocity = new Vector2(inputX * speed * currentSpeed, rigidbody.velocity.y);
        if (inputX >= 0)
        {
            mySpriteRenderer.flipX = false;
        }
        else
            mySpriteRenderer.flipX = true;


      
       





          
           
        




        /*  Vector3 newScale = gameObject.transform.localScale;
          newScale.x *= -1;
          gameObject.transform.localScale = newScale;*/





    }
       
     public void Jump()
        {

     
        if (Input.GetKeyDown(KeyCode.Space))
        {
           
           
            if (jump == false)
            {
                rigidbody.AddForce(new Vector2(0, jumpForce));
                jumpingCoroutine = StartCoroutine(Jumping());
            }
            if (jump == true)
            {

            }

        }
                
               
      
        }
    IEnumerator Jumping()
    {
        jump = true;
        yield return new WaitForSeconds(jumpRate);
        jump = false;
        
       



    }

    public void Shoot()
    {
        
        if (Input.GetButtonDown("Fire1") && Time.time >= nextFire) 
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                Debug.Log("Clicked on the UI");
            }
            else
            {
                Instantiate(bullet, transform.position, transform.rotation);
                nextFire = Time.time + fireRate;
                DamageWhenFire();
            }
          
           
           
        }
    }
  
    private void OnTriggerEnter2D(Collider2D collision)
    {
        DamageDealer damageDealer = collision.GetComponent<DamageDealer>();
        if (collision.gameObject.tag == "Enemy")
        {
            Debug.Log("Damage");
            health = health- damageDealer.GetDamage();
        }
        if (collision.gameObject.tag == "Win")
        {
            sceneLoader.LoadNextSceneWithDelay();
        }
        if (collision.gameObject.tag == "RestartLevel")
        {
            sceneLoader.LoadCurrentSceneWithDelay();
        }
        if (collision.gameObject.tag == "Coin")
        {
            score += coins.GetCoin();
            scoreText.text = score.ToString();


        }

    }

   
    private void OnCollisionEnter2D(Collision2D collision)
    {
        FastSpeed fastSpeed = collision.gameObject.GetComponent<FastSpeed>();
        if (collision.gameObject.tag == "glide")
        {
            currentSpeed = speed + fastSpeed.GetFastSpeed();
            Debug.Log(speed);
        }

       

    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "glide")
        {
            currentSpeed = speed;
            Debug.Log(speed);
        }
    }

   
}
