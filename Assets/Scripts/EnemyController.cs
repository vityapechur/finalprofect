﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    PlayerController playerController;
    [SerializeField] float speedEnemy;
    [SerializeField] float speedEnemyWandegring;
    EnemyState currentState;
    [SerializeField] float moveDistance;
    [SerializeField] float wanderingDistance = 3;

    [SerializeField] AudioClip destroySound;
    Vector2 target;

    private SpriteRenderer mySpriteRenderer;
    enum EnemyState
    {
        WANDERING,
        RUN
    }
    // Start is called before the first frame update
    
    
    
    
    void Start()
    {
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        target = new Vector2(transform.position.x + wanderingDistance, transform.position.y);
        wanderingDistance = -wanderingDistance;
        playerController = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            case EnemyState.WANDERING:
                DoWandering();
                break;
            case EnemyState.RUN:
                Run();
                break;
 
        }
    }


    public void DoWandering()
    {
        float distance = Vector2.Distance(transform.position, playerController.transform.position);
      
        if (distance > moveDistance)
        {
           
            float distanceWandering = Vector2.Distance(transform.position, target);
         
            transform.position = Vector2.MoveTowards(transform.position, target, speedEnemyWandegring * Time.deltaTime);
           
            if (distanceWandering <= 0.1f)
            {
                target = new Vector2(transform.position.x + wanderingDistance, transform.position.y);
                wanderingDistance = -wanderingDistance;
                mySpriteRenderer.flipX = !mySpriteRenderer.flipX;
            }

        }
        else
        {
          currentState= EnemyState.RUN;
            
        }
    }
    public void Run()
    {
        float distance = Vector2.Distance(transform.position, playerController.transform.position);

        if (distance < moveDistance)
        {

            Vector2 direction = playerController.transform.position - transform.position;
            Vector2 target = new Vector2(playerController.transform.position.x, transform.position.y);
            Vector2 newPosition= Vector2.MoveTowards(transform.position, target, speedEnemy * Time.deltaTime);
    
            transform.position = new Vector2(newPosition.x, newPosition.y);

        }
       
        if (distance > moveDistance)
        {
            currentState = EnemyState.WANDERING;
        }


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerBullet")
        {
            Destroy(gameObject);
            playerController.PlaySound(destroySound);
        }
        else if (collision.gameObject.tag == "Player")
        {
            Destroy(gameObject);
        }
    }



}
